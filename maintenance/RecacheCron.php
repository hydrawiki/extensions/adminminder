<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/
namespace AdminMinder;
require_once(dirname(__DIR__, 3)."/maintenance/Maintenance.php");

class RecacheCron extends \Maintenance {
	/**
	 * Main Executor
	 *
	 * @access	public
	 * @return	void
	 */
	public function execute() {
		Recache::run([], false);
	}
}

$maintClass = 'AdminMinder\RecacheCron';
require_once(RUN_MAINTENANCE_IF_MAIN);
