<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/
namespace AdminMinder;

class Recache extends \SyncService\Job {
	/**
	 * Main entry point.
	 *
	 * @param	array	the same array that was passed to self::queue()
	 * @return	integer	the exit value that this process fork should use
	 */
	public function execute($args = []) {
		// fetch list of current admins from all child wikis
		$allAdmins = AdminList::getCurrentAdmins($this->logger);

		// pull our cached list of admins out of the master DB
		$oldAdminList = AdminList::getCachedAdmins();

		// diff between the two lists, generating events for our feed
		$events = Event::createAllByDiff($oldAdminList, $allAdmins);

		// Save newly fetched list of admins to our cache table
		AdminList::saveNewAdmins($allAdmins);

		// send Benjamin Tarsa an email if this was a cron and events were generated
		if (count($events)) {
			global $wgServer;

			$benjamin = \User::newFromName('CrsBenjamin');
			$benjamin->load();
			$benjamin->sendMail(
				'AdminMinder Alert: '.count($events).' admin changes happened this week',
				"Visit $wgServer/Special:AdminMinder to see the changes and the updated email list."
			);
		}
	}

	/**
	 * Return cron schedule if applicable.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	static public function getSchedule() {
		return [
			[
				'minutes' => 0,
				'hours' => 0,
				'days' => '*',
				'months' => '*',
				'weekdays' => 0
			]
		];
	}
}
