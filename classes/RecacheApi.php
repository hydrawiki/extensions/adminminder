<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/
namespace AdminMinder;

class RecacheApi extends ListApi {
	public function getActions() {
		return array_merge(parent::getActions(), [
			'recache' => [
				'tokenRequired' => true,
				'postRequired' => true
			],
		]);
	}

	public function doRecache() {
		Recache::queue();
		$this->getResult()->addValue(null, 'result', true);
	}
}
