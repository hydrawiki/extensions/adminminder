<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/

namespace AdminMinder;

use \DynamicSettings\Wiki;

/**
 * Model for reading and writing rows in the admin_minder_events table
 * One instance represents an ADDITION or REMOVAL of a group from a user on a wiki
 */
class Event {
	/**
	 * Static cache for Wiki objects.
	 *
	 * @var		array
	 */
	static private $wikiCache = [];

	/**
	 * Static cache for User objects.
	 *
	 * @var		array
	 */
	static private $userCache = [];

	/**
	 * Global ID of the user on this event.
	 *
	 * @var		integer
	 */
	protected $globalId = null;

	/**
	 * Wiki Site Key (Not the Wiki object.)
	 *
	 * @var		string
	 */
	protected $wiki = null;

	/**
	 * Standard MediaWiki group name.  Example: sysop, bureaucrat
	 *
	 * @var		string
	 */
	protected $group = null;

	/**
	 * If the group was added in this event.  See constants.
	 *
	 * @var		integer
	 */
	protected $added = null;

	/**
	 * Date of the event.
	 *
	 * @var		string
	 */
	protected $date = null;

	/**
	 * If this event has been saved.
	 *
	 * @var		boolean
	 */
	protected $saved = false;

	const GROUP_ADDED = 1;

	const GROUP_REMOVED = 0;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @param	integer	Global User ID
	 * @param	string	Site Key
	 * @param	string	Group Name
	 * @param	integer	Group Integer Constant
	 * @param	string	[Optional] Date
	 * @return	void
	 */
	public function __construct($globalId, $wiki, $group, $added, $date = null) {
		// TODO validate inputs
		$this->globalId = intval($globalId);
		$this->wiki = $wiki;
		$this->group = $group;
		$this->added = $added == self::GROUP_ADDED;
		$this->date = ($date !== null ? $date : date('Y-m-d H:i:s'));
	}

	/**
	 * Save this event to the database.
	 *
	 * @access	public
	 * @return	void
	 */
	public function save() {
		if ($this->saved) {
			return;
		}

		$db = wfGetDB(DB_MASTER);
		$res = $db->insert(
			'admin_minder_events',
			[
				'site_key' => $this->wiki,
				'global_id' => $this->globalId,
				'added_or_removed' => intval($this->added),
				'`group`' => $this->group,
				'`date`' => $this->date,
			],
			__METHOD__
		);

		$this->saved = $res;
	}

	/**
	 * Was this added or removed?
	 *
	 * @access	public
	 * @return	boolean	True if added.
	 */
	public function wasAdded() {
		return boolval($this->added);
	}

	/**
	 * Get a Wiki object for this event.
	 *
	 * @access	public
	 * @return	mixed	Wiki object or false for not found.
	 */
	public function getWiki() {
		if (isset($this->wiki)) {
			if (isset(self::$wikiCache[$this->wiki])) {
				return self::$wikiCache[$this->wiki];
			}
			$wiki = Wiki::loadFromHash($this->wiki);
			self::$wikiCache[$this->wiki] = $wiki;
			return $wiki;
		}
		return false;
	}

	/**
	 * Group name was that added or removed.
	 *
	 * @access	public
	 * @return	string	Group Name
	 */
	public function getGroupName() {
		return $this->group;
	}

	/**
	 * Date of the event.
	 *
	 * @access	public
	 * @return	string	'Y-m-d H:i:s' time stamp.
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Get the global ID associated with this event.
	 *
	 * @access	public
	 * @return	integer	Global ID
	 */
	public function getGlobalId() {
		return intval($this->globalId);
	}

	/**
	 * Get the global ID associated with this event.
	 *
	 * @access	public
	 * @return	mixed	User object or null for not found.
	 */
	public function getUser() {
		$globalId = $this->getGlobalId();

		if (!$globalId) {
			return null;
		}

		if (isset(self::$userCache[$globalId])) {
			$user = self::$userCache[$globalId];
		} else {
			$lookup = \CentralIdLookup::factory();
			$user = $lookup->localUserFromCentralId($globalId);
		}

		return $user;
	}

	/**
	 * Performs a diff between two lists and creates events for each addition or removal.
	 *
	 * @access	public
	 * @param	array	Old cached list of admins.
	 * @param	array	Current list of admins.
	 * @return	array	List of event objects created.
	 */
	static public function createAllByDiff($oldList, $newList) {
		// list format is as follows:
		// {
		//		GLOBAL_ID: [
		//			SITE_KEY: [ GROUP_NAME, ... ],
		//			...
		//		],
		//		...
		// }

		$events = [];

		// users that have been removed entirely
		$removedUsers = array_diff(array_keys($oldList), array_keys($newList));

		// users that are newly promoted
		$addedUsers = array_diff(array_keys($newList), array_keys($oldList));

		// users that exist in both lists and need a deeper diff
		$usersToDiff = array_intersect(array_keys($newList), array_keys($oldList));
		$usersAddedToWikis = [];
		$usersRemovedFromWikis = [];
		foreach ($usersToDiff as $globalId) {
			// an existing admin lost rights on an individual wiki
			$usersRemovedFromWikis[$globalId] = array_diff(array_keys($oldList[$globalId]), array_keys($newList[$globalId]));

			// an existing admin was given rights on a new wiki
			$usersAddedToWikis[$globalId] = array_diff(array_keys($newList[$globalId]), array_keys($oldList[$globalId]));

			// find changes within remaining wikis
			$wikisToDiff = array_intersect(array_keys($oldList[$globalId]), array_keys($newList[$globalId]));
			foreach ($wikisToDiff as $md5_key) {
				// an existing admin lost rights on an individual wiki
				$groupsRemoved = array_diff(array_keys($oldList[$globalId][$md5_key]), array_keys($newList[$globalId][$md5_key]));

				// an existing admin was given rights on a new wiki
				$groupsAdded = array_diff(array_keys($newList[$globalId][$md5_key]), array_keys($oldList[$globalId][$md5_key]));

				if (count($groupsRemoved)) {
					$events = array_merge($events, self::createEventsForGroups($groupsRemoved, $globalId, $md5_key, self::GROUP_REMOVED));
				}

				if (count($groupsAdded)) {
					$events = array_merge($events, self::createEventsForGroups($groupsAdded, $globalId, $md5_key, self::GROUP_ADDED));
				}
			}
		}

		foreach ($usersRemovedFromWikis as $globalId => $removedWikis) {
			foreach ($removedWikis as $md5_key) {
				$events = array_merge($events, self::createEventsForGroups($oldList[$globalId][$md5_key], $globalId, $md5_key, self::GROUP_REMOVED));
			}
		}

		foreach ($usersAddedToWikis as $globalId => $addedWikis) {
			foreach ($addedWikis as $md5_key) {
				$events = array_merge($events, self::createEventsForGroups($newList[$globalId][$md5_key], $globalId, $md5_key, self::GROUP_ADDED));
			}
		}

		foreach ($removedUsers as $globalId) {
			foreach ($oldList[$globalId] as $md5_key => $groups) {
				$events = array_merge($events, self::createEventsForGroups($groups, $globalId, $md5_key, self::GROUP_REMOVED));
			}
		}

		foreach ($addedUsers as $globalId) {
			foreach ($newList[$globalId] as $md5_key => $groups) {
				$events = array_merge($events, self::createEventsForGroups($groups, $globalId, $md5_key, self::GROUP_ADDED));
			}
		}

		return $events;
	}

	// creates added or removed events for an array of groups belonging to a user on a wiki
	private static function createEventsForGroups($groups, $globalId, $wiki, $added) {
		if (empty($groups)) {
			return [];
		}
		$events = [];
		foreach ($groups as $group) {
			$event = new self($globalId, $wiki, $group, $added);
			$event->save();
			$events[] = $event;
		}
		return $events;
	}

	/**
	 * Get recent events with optiona offset and limit.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Offset
	 * @param	integer	[Optional] Number of items to return.
	 * @return	array	Multidimensional array of events.
	 */
	static public function getRecent($start = null, $itemsPerPage = null) {
		$options['ORDER BY'] = '`date` DESC';
		if ($start !== null) {
			$options['OFFSET'] = $start;
		}
		if ($itemsPerPage !== null) {
			$options['LIMIT'] = $itemsPerPage;
		}

		$db = wfGetDB(DB_SLAVE);
		$result = $db->select(
			['admin_minder_events'],
			['*',],
			['admin_minder_events.`date` > '.$db->addQuotes(date('Y-m-d H:i:s', strtotime('1 month ago')))],
			__METHOD__,
			$options
		);

		$events = [];
		while ($row = $result->fetchRow()) {
			$events[] = new self($row['global_id'], $row['site_key'], $row['group'], $row['added_or_removed'], $row['date']);
		}

		return $events;
	}

	/**
	 * Return the total amount of recent events.
	 *
	 * @access	public
	 * @return	integer	Total Recent Events
	 */
	static public function getRecentTotal() {
		$db = wfGetDB(DB_SLAVE);
		$result = $db->select(
			['admin_minder_events'],
			['count(`admin_minder_events`.`global_id`) as total'],
			[/*'user_name IS NOT NULL',*/ 'admin_minder_events.`date` > \''.date('Y-m-d H:i:s', strtotime('1 month ago'))."'"],
			__METHOD__
		);

		$total = $result->fetchRow();

		return intval($total['total']);
	}
}
