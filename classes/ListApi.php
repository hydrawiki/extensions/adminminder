<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/
namespace AdminMinder;

class ListApi extends \HydraApiBase {
	public static $adminGroups = ['bureaucrat', 'sysop', 'wiki_guardian', 'hydra_admin'];

	public function getDescription() {
		return 'Returns a list of admins on the wiki by global user ID.';
	}

	public function getActions() {
		return [
			'list' => [
				'tokenRequired' => false,
				'postRequired' => false,
				'params' => [
				]
			],
		];
	}

	public function doList() {
		$db = wfGetDB(DB_SLAVE);

		$res = $db->select(
			[
				'user_groups',
				'user',
				'user_global'
			],
			[
				'user_groups.ug_group',
				'user_global.global_id'
			],
			[
				'user_global.global_id > 0',
				"user_groups.ug_group" => self::$adminGroups
			],
			__METHOD__,
			['ORDER BY' => 'user_global.global_id'],
			[
				'user_global' => [
					'LEFT JOIN',
					['user_global.user_id = user_groups.ug_user']
				],
				'user' => [
					'LEFT JOIN',
					['user.user_id = user_groups.ug_user']
				]
			]
		);

		$admins = [];
		foreach ($res as $row) {
			$admins[$row->global_id][] = $row->ug_group;
		}

		$this->getResult()->addValue(null, 'admins', $admins);
	}
}
