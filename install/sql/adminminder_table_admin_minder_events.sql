CREATE TABLE /*_*/admin_minder_events (
  `event_id` int(14) NOT NULL AUTO_INCREMENT,
  `wiki_key` varchar(32) NOT NULL,
  `curse_id` int(12) NOT NULL,
  `added_or_removed` int(1) NOT NULL,
  `group` varbinary(255) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`event_id`)
) /*$wgDBTableOptions*/;
