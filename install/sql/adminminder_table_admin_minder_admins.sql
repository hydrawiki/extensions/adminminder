CREATE TABLE /*_*/admin_minder_admins (
  `admin_id` int(14) NOT NULL AUTO_INCREMENT,
  `wiki_key` varchar(32) NOT NULL,
  `curse_id` int(12) NOT NULL,
  `groups` text NOT NULL,
  PRIMARY KEY (`admin_id`)
) /*$wgDBTableOptions*/;
