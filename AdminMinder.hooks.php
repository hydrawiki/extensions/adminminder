<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/

use DynamicSettings\Environment;

class AdminMinderHooks {
	public static function onLoadExtensionSchemaUpdates($updater) {
		$extDir = __DIR__;

		if (Environment::isMasterWiki()) {
			$updater->addExtensionUpdate(['addTable', 'admin_minder_events', "{$extDir}/install/sql/adminminder_table_admin_minder_events.sql", true]);
			$updater->addExtensionUpdate(['addTable', 'admin_minder_admins', "{$extDir}/install/sql/adminminder_table_admin_minder_admins.sql", true]);

			$updater->addExtensionUpdate(['modifyField', 'admin_minder_admins', 'curse_id', "{$extDir}/upgrade/sql/admin_minder_admins/rename_curse_id.sql", true]);
			$updater->addExtensionUpdate(['modifyField', 'admin_minder_events', 'curse_id', "{$extDir}/upgrade/sql/admin_minder_events/rename_curse_id.sql", true]);
			$updater->addExtensionUpdate(['modifyField', 'admin_minder_admins', 'wiki_key', "{$extDir}/upgrade/sql/admin_minder_admins/rename_wiki_key.sql", true]);
			$updater->addExtensionUpdate(['modifyField', 'admin_minder_events', 'wiki_key', "{$extDir}/upgrade/sql/admin_minder_events/rename_wiki_key.sql", true]);
		}

		return true;
	}
}
