(function($) { $(function(){

$('.timeago').timeago();

$('#reload').click(function(event) {
	$(this).attr('disabled', true).text('Recaching...');
	(new mw.Api()).post({
		action: 'admins',
		do: 'recache',
		token: mw.user.tokens.get('editToken')
	}).always(function(resp) {
		$('#progress').slideDown();
	});
}).attr('disabled', false);

});})(jQuery);
