<?php
/**
 * Curse Inc.
 * Admin Minder
 * Admin Minder Aliases
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Admin Minder
 * @link		https://gitlab.com/hydrawiki
 *
**/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'AdminMinder' => ['AdminMinder']
];
