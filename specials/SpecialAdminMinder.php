<?php
/**
 * Curse Inc.
 * Admin Minder
 * Keeps track of admins added and removed over time on Hydra wikis
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AdminMinder
 * @link		https://gitlab.com/hydrawiki
 *
**/

use DynamicSettings\Environment;

class SpecialAdminMinder extends HydraCore\SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('AdminMinder', 'adminminder');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($path) {
		if (!Environment::isMasterWiki()) {
			throw new \ErrorPageError(wfMessage('admin_minder_error'), wfMessage('admin_minder_master_only'));
		}
		$this->checkPermissions();

		$this->getOutput()->addModules(['ext.adminminder']);

		$this->setHeaders();

		$this->adminMinderPage();

		$this->getOutput()->addHTML($this->content);
	}

	/**
	 * Build the admin minder page and set the content.
	 *
	 * @access	public
	 * @return	void
	 */
	public function adminMinderPage() {
		$start = $this->getRequest()->getInt('st');
		$itemsPerPage = 100;

		$events = AdminMinder\Event::getRecent(0, 100);
		$total = AdminMinder\Event::getRecentTotal();
		$list = AdminMinder\AdminList::getEmailList();

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start);
		
		$this->content = $this->buildOutput($list, $events, $pagination);
	}

	/**
	 * Build Output HTML
	 *
	 * @access	private
	 * @param	string	Comma delimited list of email addresses.
	 * @param	array	Multidimensional Event Data
	 * @param	string	Raw Pagination HTML
	 * @return	string	Raw HTML
	 */
	private function buildOutput($emails, $events, $pagination) {
		$html = '';

		$html .= Html::element('button', ['id' => 'reload'], wfMessage('recache_admins_now')->plain());
		$html .= Html::element('div', ['id' => 'progress'], wfMessage('reload_page')->plain());

		$html .= Html::element('h2', [], wfMessage('current_list')->plain());
		$html .= Html::element('textarea', ['readonly', 'rows' => 10], $emails);

		$html .= Html::element('h2', [], wfMessage('am_recent_changes')->plain());

		$html .= $pagination;
		foreach ($events as $event) {
			$html .= $this->eventHtml($event);
		}
		$html .= $pagination;

		return $html;
	}

	/**
	 * Build Even HTML
	 *
	 * @access	private
	 * @param	array	Event Data
	 * @return	string	Raw HTML
	 */
	private function eventHtml($event) {
		$wiki = $event->getWiki();
		$user = $event->getUser();

		$html = '';
		$eventType = wfMessage('adminminder-event-' . ($event->wasAdded() ? 'added' : 'removed'))->plain();
		$wikiLink = $wiki ? Html::element('a', ['href' => "https://{$wiki->getDomains()->getDomain()}/"], $wiki->getNameForDisplay()) : 'MISSING WIKI';

		if ($user && $user->getId()) {
			$userLink = Linker::link(Title::newFromText($user->getName(), NS_USER));
		} else {
			$userLink = Html::element('span', ['title' => 'Global ID '.$event->getGlobalID()], wfMessage('unsynced_user', $event->getGlobalID())->plain());
		}

		$content = wfMessage('adminminder-event', $userLink, $eventType, $event->getGroupName(), $wikiLink)->plain();
		$html .= Html::rawElement('div', ['class' => 'event'], $content.CurseProfile\CP::timeTag($event->getDate()));

		return $html;
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access protected
	 * @return string
	 */
	protected function getGroupName() {
		return 'users';
	}
}
